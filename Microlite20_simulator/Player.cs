﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microlite20_simulator
{
    public class Player : Entity
    {
        public Player(int strengh, int dexterity, int spirit, int level, int bonus_damage, int bonus_attack,string type_attack, int damage_dice, int number_throw, int HP_inflation, int bonus_defense) : base(strengh, dexterity, spirit, level, bonus_damage, type_attack, damage_dice, number_throw)
        {
            this.HP_Max = this.Strengh + HP_inflation + ((this.Level - 1) * HP_inflation);
            this.Melee = (this.Strengh-10)/2 + this.Level + bonus_attack;
            this.Melee_2hands = (int)Decimal.Floor((int)(((this.Strengh - 10) + this.Level) + bonus_attack));
            this.Magic = (int)Decimal.Floor((int)(this.Spirit-10)/2) + this.Level + bonus_attack;
            this.Projectile = (int)Decimal.Floor((int)((((this.Dexterity - 10) / 2) + this.Level) + bonus_attack));
            this.AC_dext = 10 + (int)Decimal.Floor((int)((((this.Dexterity - 10) / 2)) + bonus_defense));
            this.AC_spi = 10 + (int)Decimal.Floor((int)((((this.Spirit - 10) / 2)) + bonus_defense));
        }
    }
}
