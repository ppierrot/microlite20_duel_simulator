﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microlite20_simulator
{
    public static class GEntity
    {
        //seeds for random number, the synclock will avoid to break the internal implementation,in order to not start getting the same numbers from different threads
        private static readonly Random rnd = new Random();
        private static readonly object syncLock = new object();

        //generate the attack roll when trying to attack 
        public static string Attack_throw(Entity attacker)
        {
            //type of attack
            int athrow;

            //all the type of attacks available
            switch (attacker.Type_attack)
            {
                case "PROJECTILE":
                    athrow = attacker.Projectile;
                    break;
                case "MELEE2":
                    athrow = (int)attacker.Melee_2hands;
                    break;
                case "MELEE":
                    athrow = attacker.Melee;
                    break;
                case "MAGIC":
                    athrow = attacker.Magic;
                    break;
                default:
                    athrow = 0;
                    break;
            }

            lock (syncLock)
            { // synchronize random

                //roll dice 20
                int d20_throw = rnd.Next(1, 20);

                //Console.WriteLine("  jet dé attaque = {0}", d20_throw);//output for test

                //if 20, it's a critical hit overwhise it's the dice roll result plus the type of attack
                if (d20_throw == 20)
                {
                    return "CRITICAL";
                }
                else
                {
                    int result_int = d20_throw + athrow;
                    //Console.WriteLine("  résultat jet attaque = {0}", result_int);//output for test
                    return result_int.ToString(); //conversion of int to string

                }
            }
        }

        //generate the damage roll when trying to attack 
        public static int Damage_throw(Entity attacker, Boolean critic)
        {
            //instanciation of the caracteristics bonus
            List<int> bonus_car;
            bonus_car = Bonus_cara(attacker.Strengh, attacker.Dexterity, attacker.Spirit);
            
            int damage = 0;

            if (critic == false)
            {

                lock (syncLock)
                { // synchronize random
                  //roll every damage dice to be rolled
                    for (int i = 0; i < attacker.Number_throw; i++)
                    {
                        damage += rnd.Next(1, attacker.Damage_dice);
                    }
                }
                //Console.WriteLine("  résultat jet de dégâts = {0}", damage);//output for test

                if (attacker.Type_attack == "Melee" || attacker.Type_attack == "Melee2")
                {
                    damage += bonus_car[0];
                }
                else if (attacker.Type_attack == "Projectile")
                {
                    damage += bonus_car[1];
                }

            }
            else
            {
                damage = attacker.Number_throw * attacker.Damage_dice;
            }

            return damage += attacker.Bonus_damage;
        }

        //will define who is going to act first
        public static int Initiative(Entity target)
        {
            //instanciation of the caracteristics bonus
            List<int> bonus_car;
            bonus_car = Bonus_cara(target.Strengh, target.Dexterity, target.Spirit);

            lock (syncLock)
            { // synchronize random
              //dice 20 roll plus dexterity
                return (bonus_car[1] + rnd.Next(1, 20));
            }
        }
        
        //the attack result of the attacker will be challeneged by the defense class of the defender
        public static List<float> Att_v_Class(Entity attacker, Entity defender, Boolean ac_player_is_dext, Boolean player_turns)
        {
            int outcome=0;
            int attack_int;
            List<float> List_attack_damage;

            List_attack_damage = new List<float>();

            //instanciation of the caracteristics bonus for the attackant
            List<int> bonus_car_atck;
            bonus_car_atck = Bonus_cara(attacker.Strengh, attacker.Dexterity, attacker.Spirit);
            //instanciation of the caracteristics bonus for the defendant
            List<int> bonus_car_dfd;
            bonus_car_dfd = Bonus_cara(defender.Strengh, defender.Dexterity, defender.Spirit);

            //proceed to make an attack roll
            string attack = Attack_throw(attacker);
            Int32.TryParse(attack, out attack_int);//conversion into int

            //if not a critical hit on the attack dice roll, then comparing the result of said roll against the defender defense class
            if (attack != "CRITICAL")
            {
                //decided which type of AC based the attack roll of the 
                if (player_turns == true)
                {
                    if (ac_player_is_dext == false)
                    {
                        outcome = attack_int - defender.AC_spi;
                    }
                    else
                    {
                        outcome = attack_int - defender.AC_dext;

                    }
                }
                //Console.WriteLine("  attack vs armor class = {0}", outcome);//output for test
            }

            //if it's a critical roll, it's instant hit and maximum damage by multplying the damage dice by the number of rolls and adding bonus
            if (attack=="CRITICAL")
            {
                List_attack_damage.Add(attack_int);
                List_attack_damage.Add(Damage_throw(attacker, true) + bonus_car_atck[0]);
            }
            //if the attack roll is higher than the defense class, proceeding to make a damage roll
            else if(outcome>0)
            {
                List_attack_damage.Add(attack_int);
                List_attack_damage.Add(Damage_throw(attacker, false) + bonus_car_atck[0]);
            }
            //if attack roll is less than defense class, it's a miss
            else
            {
                List_attack_damage.Add(0);
                List_attack_damage.Add(0);
            }

            return List_attack_damage;
        }

        //displaying the entities characteristics 
        public static string Display_Characteristics(Entity player, Entity opponent)
        {
            string stat = ""; //caracteristic to display
            bool who = false; //which entitie's turn is it to display its characteristics
            string who_name; //name of the entitie to display
            var car_entity = new List<string>() {""}; //list which will contain every characteristics of an entity
            string final_display = ""; //final display to return, containing both entities cara's

            //for each entitie
            for (int j = 0; j < 2; j++)
            {
                //getting the proper entitie's caracteristics
                if (who == false)
                {
                    car_entity = GEntity.Get_Characteristics(player);
                    who_name = "Player";
                    who = true;
                }
                else
                {
                    car_entity.Clear(); //empty the list from previous entities cara's
                    car_entity = GEntity.Get_Characteristics(opponent);
                    who_name = "Opponent";
                }

                //showcase which entitie
                final_display += who_name + "'s stats :" + Environment.NewLine;

                //for every characteristics, at the exception of the first iteration of the list because it's actually the total number of characteristics
                for (int i = 1; i < Int32.Parse(car_entity[0]); i++)
                {
                    switch (i)
                    {
                        case 1:
                            stat = "Max HP";
                            break;
                        case 2:
                            stat = "Level";
                            break;
                        case 3:
                            stat = "Strengh";
                            break;
                        case 4:
                            stat = "Spirit";
                            break;
                        case 5:
                            stat = "Dexterity";
                            break;
                        case 6:
                            stat = "Melee";
                            break;
                        case 7:
                            stat = "Melee with 2 hands";
                            break;
                        case 8:
                            stat = "Magic";
                            break;
                        case 9:
                            stat = "Projectile";
                            break;
                        case 10:
                            stat = "Armor class (dexterity)";
                            break;
                        case 11:
                            stat = "Armor class (spirit)";
                            break;
                        case 12:
                            stat = "Type of attack";
                            break;
                        case 13:
                            stat = "Dice for damage";
                            break;
                        case 14:
                            stat = "Number of damage dices thrown";
                            break;
                    }
                    final_display += stat + " : " + car_entity[i] + Environment.NewLine;

                    //carriage return to separate each entities caracteristics display
                    if(i == Int32.Parse(car_entity[0])-1)
                    {
                        final_display += Environment.NewLine;
                    }
                }
            }

            return final_display;
        }

        public static List<string> Get_Characteristics(Entity entity)
        {
            List<string> List_car; //list of every characteristics values, following by their respectiv value in the next iteration 
            int nbr = 15; //total amount of values in the list, this same value included

            List_car = new List<string>();

            List_car.Add(nbr.ToString());//first value of the list is the total amount of characteristics

            //getting every characteristics from the entity in argument
            for (int i = 1; i < nbr; i++)
            {
                switch (i)
                {
                    case 1:
                        List_car.Add(entity.HP_Max.ToString());
                        break;
                    case 2:
                        List_car.Add(entity.Level.ToString());
                        break;
                    case 3:
                        List_car.Add(entity.Strengh.ToString());
                        break;
                    case 4:
                        List_car.Add(entity.Dexterity.ToString());
                        break;
                    case 5:
                        List_car.Add(entity.Spirit.ToString());
                        break;
                    case 6:
                        List_car.Add(entity.Melee.ToString());
                        break;
                    case 7:
                        List_car.Add(entity.Melee_2hands.ToString());
                        break;
                    case 8:
                        List_car.Add(entity.Magic.ToString());
                        break;
                    case 9:
                        List_car.Add(entity.Projectile.ToString());
                        break;
                    case 10:
                        List_car.Add(entity.AC_dext.ToString());
                        break;
                    case 11:
                        List_car.Add(entity.AC_spi.ToString());
                        break;
                    case 12:
                        List_car.Add(entity.Type_attack);
                        break;
                    case 13:
                        List_car.Add(entity.Damage_dice.ToString());
                        break;
                    case 14:
                        List_car.Add(entity.Number_throw.ToString());
                        break;
                }
            }
            return List_car;
        }

        //create a list of caracteristics bonus from the caracteristic scores
        public static List<int> Bonus_cara(int str, int dext, int spr)
        {
            List<int> bonus_car = new List<int>();
            bonus_car.Add((int)Decimal.Floor((int)(str-10)/2));
            bonus_car.Add((int)Decimal.Floor((int)(dext - 10) / 2));
            bonus_car.Add((int)Decimal.Floor((int)(spr - 10) / 2));
            return bonus_car;
        } 

        //displaying a report of who is attacking and the result of the attack
        public static List<string> Round_report(bool turn, List<float> result)
        {
            List<string> actions = new List<string>();

            //who is making the action
            if (turn == true)
            {
                actions.Add("Player");
                actions.Add("Opponent");
            }
            else
            {
                actions.Add("Opponent");
                actions.Add("Player");
            }

            //adding the result of the attack roll in the result
            actions.Add(result[0].ToString());

            //if the attack roll is stronger than the defender class, damage are added in the list, otherwhise it's a miss added to the list
            if (result[0] > 0)
            {
                actions.Add(result[1].ToString());
            }
            else
            {
                actions.Add("missed");
            }

            return actions;
        } 
        
    }
}
