﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using System.IO;

namespace Microlite20_simulator
{
    public partial class Microlite20_simulator : Form
    {
        bool turn; //who's turn it is
        int proba; //final probability for a fighter to win 
        int nbr_test = 1; //how many battle test to do
        List<float> var_damage; //contain  the result of attack roll and damage roll
        List<string> var_report; //contain who's attacking, defending, the attacker damage roll, if the latter one missed or the damage he inflict
        int Player_wins = 0;
        int Opponent_wins = 0;
        List<Entity> entities; //contain every object entity created through each simulation

        // Declare a new memory stream.
        MemoryStream userInput = new MemoryStream();

        public Microlite20_simulator()
        {
            InitializeComponent();
            entities = new List<Entity>();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Buttons
        /// </summary>

        //start simulation
        private void Simulation_button_Click(object sender, EventArgs e)
        {
            try
            {
                //instanciation of player and opponent entities object
                entities.Add(new Player(Decimal.ToInt32(strengh_player_input.Value), Decimal.ToInt32(spirit_player_input.Value), Decimal.ToInt32(dext_player_input.Value), Decimal.ToInt32(lvl_player_input.Value), Decimal.ToInt32(dmgbonus_player_input.Value), Decimal.ToInt32(bonusatck_player_input.Value), typeAt_player_combo.Text.ToString(), Int32.Parse(dmgDice_player_combo.Text), Decimal.ToInt32(diceDmg_player_input.Value), Decimal.ToInt32(HealthInflation_player_input.Value), Decimal.ToInt32(bonusCls_player_input.Value)));
                entities.Add(new Opponent(Decimal.ToInt32(strengh_oppo_input.Value), Decimal.ToInt32(spirit_oppo_input.Value), Decimal.ToInt32(dext_oppo_input.Value), Decimal.ToInt32(lvl_oppo_input.Value), Decimal.ToInt32(bonusDmg_oppo_input.Value), Decimal.ToInt32(bonusAtck_oppo_input.Value), atckType_oppo_combo.Text.ToString(), Int32.Parse(dmgDice_oppo_combo.Text), Decimal.ToInt32(nbrDmgDice_oppo_input.Value), Decimal.ToInt32(health_oppo_input.Value), Decimal.ToInt32(melee_oppo_input.Value), Decimal.ToInt32(magic_oppo_input.Value), Decimal.ToInt32(proj_oppo_input.Value), Decimal.ToInt32(AC_oppo_input.Value), Decimal.ToInt32(AC_oppo_input.Value)));

                //get the user's input of number of tests
                nbr_test = Convert.ToInt32(numberTest_input.Value);

                //creation of various lists
                var_damage = new List<float>();
                var_report = new List<string>();

                //empty the ouput before new simulation
                output_simulation.Text = null;
                //reset probabilities
                Player_wins = 0;
                Opponent_wins = 0;
                //display the characteristics of each entities
                output_simulation.Text = GEntity.Display_Characteristics(entities[entities.Count - 2], entities[entities.Count - 1]);

                //simulation begin
                for (int i = 0; i < nbr_test; i++)
                {
                    //definition of the fighters health points
                    int hp_player = entities[entities.Count - 2].HP_Max;
                    int hp_opponent = entities[entities.Count - 1].HP_Max;

                    //definition of the player's choice between dexterity or spirit on AC formula
                    Boolean player_AC_is_dext = false;
                    if (ACbase_player_combo.Text == "dex")
                    {
                        player_AC_is_dext = true;
                    }
                
                    //definition of the initiative roll of each fighters
                    int init_player = GEntity.Initiative(entities[entities.Count - 2]);
                    int init_opponent = GEntity.Initiative(entities[entities.Count - 1]);

                    //display battle id
                    output_simulation.Text += "Battle " + (i+1) + Environment.NewLine;

                    //decide who's acting first
                    if (init_player > init_opponent)
                    {
                        turn = true;
                    }
                    else
                    {
                        turn = false;
                    }

                    int j = 0;

                    //as long as every fighters is not defeated (0 hp)
                    while (hp_player > 0 && hp_opponent > 0)
                    {
                        //display round number x
                        if (j == 0 || j % 2 == 0)
                        {
                            output_simulation.Text += Environment.NewLine + " Round " + (j / 2 + 1) + Environment.NewLine;
                        }

                        //the one fighter having the initiative will attack
                        if (turn == true)
                        {
                            var_damage = GEntity.Att_v_Class(entities[entities.Count - 2], entities[entities.Count - 1], player_AC_is_dext, true);
                        }
                        else
                        {
                            var_damage = GEntity.Att_v_Class(entities[entities.Count - 2], entities[entities.Count - 1], player_AC_is_dext, false);
                        }

                        //reporting the result of the confrontation between the attacker's attack roll and the defender's defense class
                        var_report = GEntity.Round_report(turn, var_damage);

                        //display who attack who
                        output_simulation.Text += " " + var_report[0] + " attack " + var_report[1] + Environment.NewLine;

                        //if the attacker damage roll is higher, displaying the value of the latter one and the damage inflicted
                        if (var_report[3] != "missed")
                        {
                            output_simulation.Text += " " + var_report[0] + " made a " + var_report[2] + " on his attack roll";
                            output_simulation.Text += ", " + var_report[0] + " inflicted " + var_report[3] + " damage to " + var_report[1] + Environment.NewLine;
                            
                            //substract the damage from the defenser health points
                            if (turn == true)
                            {
                                hp_opponent -= Int32.Parse(var_report[3]);
                            }
                            else
                            {
                                hp_player -= Int32.Parse(var_report[3]);
                            }
                        }
                        else
                        {
                            //display if the attack missed
                            output_simulation.Text += " " + var_report[0] + " " + var_report[3] + Environment.NewLine;
                        }

                        //display of the fighters health points
                        output_simulation.Text += " Player's HP = " + hp_player + Environment.NewLine;
                        output_simulation.Text += " Opponent's HP = " + hp_opponent + Environment.NewLine;

                        //if a fighter hit 0 health points, the winner get the round
                        if (hp_player <= 0)
                        {
                            Opponent_wins += 1;
                            output_simulation.Text += "Opponent wins" + Environment.NewLine + Environment.NewLine;
                        }
                        else if (hp_opponent <= 0)
                        {
                            Player_wins += 1;
                            output_simulation.Text += "Player wins" + Environment.NewLine + Environment.NewLine;
                        }

                        //switching who's turn to make an attack
                        if (turn == true)
                        {
                            turn = false;
                        }
                        else
                        {
                            turn = true;
                        }

                        j++;
                    }
                }
                
                //displaying the results of the simulation by calculing the probaibility of winning of each fighters in such scenario
                proba = Player_wins * 100 / nbr_test;
                output_simulation.Text += Environment.NewLine + "Player has a " + proba + "% probability of winning" + Environment.NewLine;
                proba = Opponent_wins * 100 / nbr_test;
                output_simulation.Text += "Opponent has a " + proba + "% probability of winning" + Environment.NewLine;
            }
            catch
            {
                MessageBox.Show("Des caractéristiques sont érronées ou absentes.", "Erreur simulation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //set all the forms into their minimal value or emptying them
        private void Empty_button_Click(object sender, EventArgs e)
        {
            output_simulation.Text = null;
            typeAt_player_combo.Text = null;
            dmgDice_player_combo.Text = null;
            health_player_display.Text = "1";
            dmgDice_oppo_combo.Text = null;
            atckType_oppo_combo.Text = null;
            strengh_player_input.Value = strengh_player_input.Minimum;
            dext_player_input.Value = dext_player_input.Minimum;
            spirit_player_input.Value = spirit_player_input.Minimum;
            lvl_player_input.Value = lvl_player_input.Minimum;
            dmgbonus_player_input.Value = 0;
            bonusatck_player_input.Value = 0;
            bonusCls_player_input.Value= 0;
            diceDmg_player_input.Value = diceDmg_player_input.Minimum;
            strengh_oppo_input.Value = strengh_oppo_input.Minimum;
            dext_oppo_input.Value = dext_oppo_input.Minimum;
            spirit_oppo_input.Value = spirit_oppo_input.Minimum;
            lvl_oppo_input.Value = lvl_oppo_input.Minimum;
            bonusDmg_oppo_input.Value = bonusDmg_oppo_input.Minimum;
            bonusAtck_oppo_input.Value = bonusAtck_oppo_input.Minimum;
            nbrDmgDice_oppo_input.Value = nbrDmgDice_oppo_input.Minimum;
            health_oppo_input.Value = health_oppo_input.Minimum;
            melee_oppo_input.Value = melee_oppo_input.Minimum;
            proj_oppo_input.Value = proj_oppo_input.Minimum;
            magic_oppo_input.Value = magic_oppo_input.Minimum;
            AC_oppo_input.Value = AC_oppo_input.Minimum;
            numberTest_input.Value = numberTest_input.Minimum;
            HealthInflation_player_input.Value = HealthInflation_player_input.Minimum;
            atckType_oppo_combo.Text = null;
            Melee_player_display.Text = "-";
            melee2_player_display.Text = "-";
            proj_player_display.Text = "-";
            magic_player_display.Text = "-";
            ac_player_display.Text = "-";
            numberTest_input.Value = numberTest_input.Minimum;
        }

        //save the result of the simulation in .txt format in the current directory 
        private void Save_button_Click(object sender, EventArgs e)
        {
            if(!(output_simulation.Text == "" || output_simulation.Text == " " || output_simulation.Text == null))
            {
                output_simulation.SaveFile(userInput, RichTextBoxStreamType.PlainText);
                userInput.WriteByte(13);

                // Display the entire contents of the stream,
                // by setting its position to 0, to RichTextBox2.
                userInput.Position = 0;
                output_simulation.LoadFile(userInput, RichTextBoxStreamType.PlainText);

                // Set the properties on SaveFileDialog1 so the user is 
                // prompted to create the file if it doesn't exist 
                // or overwrite the file if it does exist.
                save.CreatePrompt = true;
                save.OverwritePrompt = true;

                // Set the file name to myText.txt, set the type filter
                // to text files, and set the initial directory to the 
                // MyDocuments folder.
                save.FileName = "microlite20_simulation_report";
                // DefaultExt is only used when "All files" is selected from 
                // the filter box and no extension is specified by the user.
                save.DefaultExt = "txt";
                save.Filter =
                    "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                save.InitialDirectory =
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                // Call ShowDialog and check for a return value of DialogResult.OK,
                // which indicates that the file was saved. 
                DialogResult result = save.ShowDialog();
                Stream fileStream;

                if (result == DialogResult.OK)
                {
                    // Open the file, copy the contents of memoryStream to fileStream,
                    // and close fileStream. Set the memoryStream.Position value to 0 
                    // to copy the entire stream. 
                    fileStream = save.OpenFile();
                    userInput.Position = 0;
                    userInput.WriteTo(fileStream);
                    fileStream.Close();
                }
            }
            else
            {
                MessageBox.Show("Aucun rapport de simulation détécté.", "Sortie vide", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Inputs
        /// </summary>

        private void Strengh_player_input_ValueChanged(object sender, EventArgs e)
        {
            int health_increase = Convert.ToInt32(HealthInflation_player_input.Value);
            int level = Convert.ToInt32(lvl_player_input.Value);
            int strengh = Convert.ToInt32(strengh_player_input.Value);
            int bonus_atk = Convert.ToInt32(bonusatck_player_input.Value);
            health_player_display.Text = HealthCalculation(strengh, health_increase, level).ToString();
            Melee_player_display.Text = MeleeCalculation(strengh, level, bonus_atk).ToString();
            melee2_player_display.Text = Melee2Calculation(strengh, level, bonus_atk).ToString();
        }

        private void Dext_player_input_ValueChanged(object sender, EventArgs e)
        {
            int level = Convert.ToInt32(lvl_player_input.Value);
            int dext = Convert.ToInt32(dext_player_input.Value);
            int bonus = Convert.ToInt32(bonusCls_player_input.Value);
            int bonus_atk = Convert.ToInt32(bonusatck_player_input.Value);
            proj_player_display.Text = ProjCalculation(dext, level, bonus_atk).ToString();
            if (ACbase_player_combo.Text == "dex")
            {
                ac_player_display.Text = ACalculation_dext(dext, bonus).ToString();
            }
            
        }

        private void Spirit_player_input_ValueChanged(object sender, EventArgs e)
        {
            int level = Convert.ToInt32(lvl_player_input.Value);
            int spirit = Convert.ToInt32(spirit_player_input.Value);
            int bonus = Convert.ToInt32(bonusCls_player_input.Value);
            int bonus_atk = Convert.ToInt32(bonusatck_player_input.Value);
            magic_player_display.Text = MagCalculation(spirit, level, bonus_atk).ToString();
            if (ACbase_player_combo.Text == "spi")
            {
                ac_player_display.Text = ACalculation_dext(spirit, bonus).ToString();
            }
        }

        private void Lvl_player_input_ValueChanged(object sender, EventArgs e)
        {
            int level = Convert.ToInt32(lvl_player_input.Value);
            int spirit = Convert.ToInt32(spirit_player_input.Value);
            int bonus = Convert.ToInt32(bonusCls_player_input.Value);
            int dext = Convert.ToInt32(dext_player_input.Value);
            int strengh = Convert.ToInt32(strengh_player_input.Value);
            int health_increase = Convert.ToInt32(HealthInflation_player_input.Value);
            int bonus_atk = Convert.ToInt32(bonusatck_player_input.Value);
            magic_player_display.Text = MagCalculation(spirit, level, bonus_atk).ToString();
            proj_player_display.Text = ProjCalculation(dext, level, bonus_atk).ToString();
            health_player_display.Text = HealthCalculation(strengh, health_increase, level).ToString();
            Melee_player_display.Text = MeleeCalculation(strengh, level, bonus_atk).ToString();
            melee2_player_display.Text = Melee2Calculation(strengh, level, bonus_atk).ToString();
        }

        private void BonusCls_player_input_ValueChanged(object sender, EventArgs e)
        {
            int bonus = Convert.ToInt32(bonusCls_player_input.Value);
            int dext = Convert.ToInt32(dext_player_input.Value);
            
        }

        private void HealthInflation_player_input_ValueChanged(object sender, EventArgs e)
        {
            int health_inflation = Convert.ToInt32(HealthInflation_player_input.Value);
            int level = Convert.ToInt32(lvl_player_input.Value);
            int strengh = Convert.ToInt32(strengh_player_input.Value);
            health_player_display.Text = HealthCalculation(strengh, health_inflation, level).ToString();
        }

        private void Bonusatck_player_input_ValueChanged(object sender, EventArgs e)
        {
            int level = Convert.ToInt32(lvl_player_input.Value);
            int spirit = Convert.ToInt32(spirit_player_input.Value);
            int bonus_atk = Convert.ToInt32(bonusatck_player_input.Value);
            int dext = Convert.ToInt32(dext_player_input.Value);
            int strengh = Convert.ToInt32(strengh_player_input.Value);
            Melee_player_display.Text = MeleeCalculation(strengh, level, bonus_atk).ToString();
            melee2_player_display.Text = Melee2Calculation(strengh, level, bonus_atk).ToString();
            proj_player_display.Text = ProjCalculation(dext, level, bonus_atk).ToString();
            magic_player_display.Text = MagCalculation(spirit, level, bonus_atk).ToString();
        }

        private void Output_simulation_TextChanged(object sender, EventArgs e)
        {
            // set the current caret position to the end
            output_simulation.SelectionStart = output_simulation.Text.Length;
            // scroll it automatically
            output_simulation.ScrollToCaret();
        }

        /// <summary>
        /// Methods
        /// </summary>

        //calculate the health points of the player
        private int HealthCalculation(int strengh, int health_inflation, int level)
        {
            return strengh + health_inflation + ((level - 1) * health_inflation);
        }

        //calculate the melee of an entity
        private int MeleeCalculation(int strengh, int level, int bonus)
        {
            return (int)Decimal.Floor((int)(strengh - 10)/2) + level + bonus;
        }

        //calculate the melee of an entity
        private int Melee2Calculation(int strengh, int level, int bonus)
        {
            return (int)Decimal.Floor((int)(strengh - 10)) + level + bonus;
        }

        //calculate the projectile of an entity
        private int ProjCalculation(int dext, int level, int bonus)
        {
            return (int)Decimal.Floor((int)(dext - 10) / 2) + level + bonus;
        }

        //calculate armor class based on dexterity of an entity
        private int ACalculation_dext(int dext, int bonus)
        {
            return (int)Decimal.Floor((int)(dext - 10) / 2) + 10 + bonus;
        }

        //calculate armor class based on dexterity of an entity
        private int ACalculation_spi(int spi, int bonus)
        {
            return (int)Decimal.Floor((int)(spi - 10) / 2) + 10 + bonus;
        }

        //calculate the magic of an entity
        private int MagCalculation(int spirit, int level, int bonus)
        {
            return (int)Decimal.Floor((int)(spirit - 10) / 2) + level + bonus;
        }

        private void Melee_player_display_Click(object sender, EventArgs e)
        {

        }

        //switch between dexterity and spirit to change AC calculation
        private void ACbase_player_combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int spirit = Convert.ToInt32(spirit_player_input.Value);
            int dext = Convert.ToInt32(dext_player_input.Value);
            int bonus = Convert.ToInt32(bonusCls_player_input.Value);
            if (ACbase_player_combo.Text == "spi")
            {
                ac_player_display.Text = ACalculation_spi(spirit, bonus).ToString();
            }
            else
            {
                ac_player_display.Text = ACalculation_dext(dext, bonus).ToString();
            }
        }
    }
}
