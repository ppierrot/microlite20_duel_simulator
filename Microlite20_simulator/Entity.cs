﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microlite20_simulator
{
    public class Entity
    {
        #region Attributs
        public int Strengh { get; set; }
        public int Dexterity { get; set; }
        public int Spirit { get; set; }
        public int Level { get; set; }
        public int HP_Max { get; set; }
        public int Melee { get; set; }
        public double Melee_2hands { get; set; }
        public int Magic { get; set; }
        public int Projectile { get; set; }
        public int AC { get; set; }
        public int MC { get; set; }
        public string Type_attack { get; set; }
        List<string> List_type_attack { get; set; }
        public int Damage_dice { get; set; }
        List<int> List_damage_dice { get; set; }
        public int Number_throw { get; set; }
        public int Bonus_damage { get; set; }
        public int AC_dext { get; set; }
        public int AC_spi { get; set; }
        #endregion

        public Entity(int strengh, int dexterity, int spirit, int level, int bonus_damage, string type_attack, int damage_dice, int number_throw)
        {
            this.Level = level;
            this.Strengh = strengh;
            this.Dexterity = dexterity;
            this.Spirit = spirit;
            this.HP_Max = 0;
            this.Melee = 0;
            this.Melee_2hands = 0;
            this.Magic = 0;
            this.Projectile = 0;
            this.AC_dext = 0;
            this.AC_spi = 0;
            this.Type_attack = type_attack;
            this.Damage_dice = damage_dice;
            this.Number_throw = number_throw;
            this.Bonus_damage = bonus_damage;
        }
    }
}
