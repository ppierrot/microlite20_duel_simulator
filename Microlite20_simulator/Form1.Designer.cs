﻿namespace Microlite20_simulator
{
    partial class Microlite20_simulator
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.strengh_player_label = new System.Windows.Forms.Label();
            this.player_section_title = new System.Windows.Forms.Label();
            this.dexterity_player_label = new System.Windows.Forms.Label();
            this.spirit_player_label = new System.Windows.Forms.Label();
            this.level_player_label = new System.Windows.Forms.Label();
            this.bonusDam_player_label = new System.Windows.Forms.Label();
            this.bonusAt_player_label = new System.Windows.Forms.Label();
            this.bonusArm_player_label = new System.Windows.Forms.Label();
            this.typeAt_player_label = new System.Windows.Forms.Label();
            this.typeAt_player_combo = new System.Windows.Forms.ComboBox();
            this.dmgDice_palyer_label = new System.Windows.Forms.Label();
            this.dmgDice_player_combo = new System.Windows.Forms.ComboBox();
            this.NbrDmgDice_player_label = new System.Windows.Forms.Label();
            this.melee_player_label = new System.Windows.Forms.Label();
            this.Melee_player_display = new System.Windows.Forms.Label();
            this.health_player_label = new System.Windows.Forms.Label();
            this.health_player_display = new System.Windows.Forms.Label();
            this.melee2_player_label = new System.Windows.Forms.Label();
            this.melee2_player_display = new System.Windows.Forms.Label();
            this.proj_player_label = new System.Windows.Forms.Label();
            this.proj_player_display = new System.Windows.Forms.Label();
            this.magic_player_label = new System.Windows.Forms.Label();
            this.magic_player_display = new System.Windows.Forms.Label();
            this.ac_player_label = new System.Windows.Forms.Label();
            this.ac_player_display = new System.Windows.Forms.Label();
            this.separator_player = new System.Windows.Forms.Label();
            this.opponent_section_title = new System.Windows.Forms.Label();
            this.separator_opponent = new System.Windows.Forms.Label();
            this.nbrDmgDice_oppo_label = new System.Windows.Forms.Label();
            this.dmgDice_oppo_combo = new System.Windows.Forms.ComboBox();
            this.dmgDice_oppo_label = new System.Windows.Forms.Label();
            this.atckType_oppo_combo = new System.Windows.Forms.ComboBox();
            this.atckType_oppo_label = new System.Windows.Forms.Label();
            this.atckBonus_oppo_label = new System.Windows.Forms.Label();
            this.dmgBonus_oppo_label = new System.Windows.Forms.Label();
            this.level_oppo_label = new System.Windows.Forms.Label();
            this.spirit_oppo_label = new System.Windows.Forms.Label();
            this.dext_oppo_label = new System.Windows.Forms.Label();
            this.strengh_oppo_label = new System.Windows.Forms.Label();
            this.health_oppo_label = new System.Windows.Forms.Label();
            this.melee_oppo_label = new System.Windows.Forms.Label();
            this.proj_oppo_label = new System.Windows.Forms.Label();
            this.magic_oppo_label = new System.Windows.Forms.Label();
            this.AC_oppo_label = new System.Windows.Forms.Label();
            this.nbr_test_label = new System.Windows.Forms.Label();
            this.separator_test = new System.Windows.Forms.Label();
            this.output_simulation = new System.Windows.Forms.RichTextBox();
            this.simulation_label = new System.Windows.Forms.Label();
            this.empty_button = new System.Windows.Forms.Button();
            this.simulation_button = new System.Windows.Forms.Button();
            this.save_button = new System.Windows.Forms.Button();
            this.strengh_player_input = new System.Windows.Forms.NumericUpDown();
            this.dext_player_input = new System.Windows.Forms.NumericUpDown();
            this.spirit_player_input = new System.Windows.Forms.NumericUpDown();
            this.lvl_player_input = new System.Windows.Forms.NumericUpDown();
            this.dmgbonus_player_input = new System.Windows.Forms.NumericUpDown();
            this.bonusatck_player_input = new System.Windows.Forms.NumericUpDown();
            this.bonusCls_player_input = new System.Windows.Forms.NumericUpDown();
            this.diceDmg_player_input = new System.Windows.Forms.NumericUpDown();
            this.strengh_oppo_input = new System.Windows.Forms.NumericUpDown();
            this.dext_oppo_input = new System.Windows.Forms.NumericUpDown();
            this.spirit_oppo_input = new System.Windows.Forms.NumericUpDown();
            this.lvl_oppo_input = new System.Windows.Forms.NumericUpDown();
            this.bonusDmg_oppo_input = new System.Windows.Forms.NumericUpDown();
            this.bonusAtck_oppo_input = new System.Windows.Forms.NumericUpDown();
            this.nbrDmgDice_oppo_input = new System.Windows.Forms.NumericUpDown();
            this.health_oppo_input = new System.Windows.Forms.NumericUpDown();
            this.melee_oppo_input = new System.Windows.Forms.NumericUpDown();
            this.proj_oppo_input = new System.Windows.Forms.NumericUpDown();
            this.magic_oppo_input = new System.Windows.Forms.NumericUpDown();
            this.AC_oppo_input = new System.Windows.Forms.NumericUpDown();
            this.numberTest_input = new System.Windows.Forms.NumericUpDown();
            this.HealthInflation_player_label = new System.Windows.Forms.Label();
            this.HealthInflation_player_input = new System.Windows.Forms.NumericUpDown();
            this.save = new System.Windows.Forms.SaveFileDialog();
            this.grade_player_label = new System.Windows.Forms.Label();
            this.grade_oppo_label = new System.Windows.Forms.Label();
            this.grade_player_combo = new System.Windows.Forms.ComboBox();
            this.grade_oppo_combo = new System.Windows.Forms.ComboBox();
            this.ACbase_player_label = new System.Windows.Forms.Label();
            this.ACbase_player_combo = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.strengh_player_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dext_player_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spirit_player_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvl_player_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dmgbonus_player_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bonusatck_player_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bonusCls_player_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceDmg_player_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strengh_oppo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dext_oppo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spirit_oppo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvl_oppo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bonusDmg_oppo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bonusAtck_oppo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbrDmgDice_oppo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.health_oppo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.melee_oppo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.proj_oppo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.magic_oppo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AC_oppo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberTest_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HealthInflation_player_input)).BeginInit();
            this.SuspendLayout();
            // 
            // strengh_player_label
            // 
            this.strengh_player_label.AutoSize = true;
            this.strengh_player_label.Location = new System.Drawing.Point(12, 45);
            this.strengh_player_label.Name = "strengh_player_label";
            this.strengh_player_label.Size = new System.Drawing.Size(44, 13);
            this.strengh_player_label.TabIndex = 1;
            this.strengh_player_label.Text = "Strengh";
            // 
            // player_section_title
            // 
            this.player_section_title.AutoSize = true;
            this.player_section_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.player_section_title.Location = new System.Drawing.Point(11, 9);
            this.player_section_title.Name = "player_section_title";
            this.player_section_title.Size = new System.Drawing.Size(58, 20);
            this.player_section_title.TabIndex = 2;
            this.player_section_title.Text = "Player";
            // 
            // dexterity_player_label
            // 
            this.dexterity_player_label.AutoSize = true;
            this.dexterity_player_label.Location = new System.Drawing.Point(12, 71);
            this.dexterity_player_label.Name = "dexterity_player_label";
            this.dexterity_player_label.Size = new System.Drawing.Size(48, 13);
            this.dexterity_player_label.TabIndex = 3;
            this.dexterity_player_label.Text = "Dexterity";
            // 
            // spirit_player_label
            // 
            this.spirit_player_label.AutoSize = true;
            this.spirit_player_label.Location = new System.Drawing.Point(12, 95);
            this.spirit_player_label.Name = "spirit_player_label";
            this.spirit_player_label.Size = new System.Drawing.Size(30, 13);
            this.spirit_player_label.TabIndex = 5;
            this.spirit_player_label.Text = "Spirit";
            // 
            // level_player_label
            // 
            this.level_player_label.AutoSize = true;
            this.level_player_label.Location = new System.Drawing.Point(12, 121);
            this.level_player_label.Name = "level_player_label";
            this.level_player_label.Size = new System.Drawing.Size(33, 13);
            this.level_player_label.TabIndex = 7;
            this.level_player_label.Text = "Level";
            // 
            // bonusDam_player_label
            // 
            this.bonusDam_player_label.AutoSize = true;
            this.bonusDam_player_label.Location = new System.Drawing.Point(118, 45);
            this.bonusDam_player_label.Name = "bonusDam_player_label";
            this.bonusDam_player_label.Size = new System.Drawing.Size(80, 13);
            this.bonusDam_player_label.TabIndex = 9;
            this.bonusDam_player_label.Text = "Damage Bonus";
            // 
            // bonusAt_player_label
            // 
            this.bonusAt_player_label.AutoSize = true;
            this.bonusAt_player_label.Location = new System.Drawing.Point(118, 71);
            this.bonusAt_player_label.Name = "bonusAt_player_label";
            this.bonusAt_player_label.Size = new System.Drawing.Size(71, 13);
            this.bonusAt_player_label.TabIndex = 11;
            this.bonusAt_player_label.Text = "Attack Bonus";
            // 
            // bonusArm_player_label
            // 
            this.bonusArm_player_label.AutoSize = true;
            this.bonusArm_player_label.Location = new System.Drawing.Point(118, 95);
            this.bonusArm_player_label.Name = "bonusArm_player_label";
            this.bonusArm_player_label.Size = new System.Drawing.Size(65, 13);
            this.bonusArm_player_label.TabIndex = 13;
            this.bonusArm_player_label.Text = "Class Bonus";
            this.bonusArm_player_label.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // typeAt_player_label
            // 
            this.typeAt_player_label.AutoSize = true;
            this.typeAt_player_label.Location = new System.Drawing.Point(118, 121);
            this.typeAt_player_label.Name = "typeAt_player_label";
            this.typeAt_player_label.Size = new System.Drawing.Size(65, 13);
            this.typeAt_player_label.TabIndex = 15;
            this.typeAt_player_label.Text = "Attack Type";
            // 
            // typeAt_player_combo
            // 
            this.typeAt_player_combo.FormattingEnabled = true;
            this.typeAt_player_combo.Items.AddRange(new object[] {
            "MELEE",
            "MELEE2",
            "PROJECTILE",
            "MAGIC"});
            this.typeAt_player_combo.Location = new System.Drawing.Point(204, 118);
            this.typeAt_player_combo.Name = "typeAt_player_combo";
            this.typeAt_player_combo.Size = new System.Drawing.Size(91, 21);
            this.typeAt_player_combo.TabIndex = 17;
            this.typeAt_player_combo.Text = "MELEE";
            // 
            // dmgDice_palyer_label
            // 
            this.dmgDice_palyer_label.AutoSize = true;
            this.dmgDice_palyer_label.Location = new System.Drawing.Point(308, 45);
            this.dmgDice_palyer_label.Name = "dmgDice_palyer_label";
            this.dmgDice_palyer_label.Size = new System.Drawing.Size(72, 13);
            this.dmgDice_palyer_label.TabIndex = 18;
            this.dmgDice_palyer_label.Text = "Damage Dice";
            // 
            // dmgDice_player_combo
            // 
            this.dmgDice_player_combo.FormattingEnabled = true;
            this.dmgDice_player_combo.Items.AddRange(new object[] {
            "4",
            "6",
            "8",
            "10",
            "12",
            "20",
            "100"});
            this.dmgDice_player_combo.Location = new System.Drawing.Point(449, 42);
            this.dmgDice_player_combo.Name = "dmgDice_player_combo";
            this.dmgDice_player_combo.Size = new System.Drawing.Size(41, 21);
            this.dmgDice_player_combo.TabIndex = 19;
            this.dmgDice_player_combo.Text = "4";
            // 
            // NbrDmgDice_player_label
            // 
            this.NbrDmgDice_player_label.AutoSize = true;
            this.NbrDmgDice_player_label.Location = new System.Drawing.Point(308, 71);
            this.NbrDmgDice_player_label.Name = "NbrDmgDice_player_label";
            this.NbrDmgDice_player_label.Size = new System.Drawing.Size(108, 13);
            this.NbrDmgDice_player_label.TabIndex = 20;
            this.NbrDmgDice_player_label.Text = "Number damage dice";
            // 
            // melee_player_label
            // 
            this.melee_player_label.AutoSize = true;
            this.melee_player_label.Location = new System.Drawing.Point(512, 45);
            this.melee_player_label.Name = "melee_player_label";
            this.melee_player_label.Size = new System.Drawing.Size(36, 13);
            this.melee_player_label.TabIndex = 24;
            this.melee_player_label.Text = "Melee";
            // 
            // Melee_player_display
            // 
            this.Melee_player_display.AutoSize = true;
            this.Melee_player_display.Location = new System.Drawing.Point(597, 45);
            this.Melee_player_display.Name = "Melee_player_display";
            this.Melee_player_display.Size = new System.Drawing.Size(10, 13);
            this.Melee_player_display.TabIndex = 25;
            this.Melee_player_display.Text = "-";
            this.Melee_player_display.Click += new System.EventHandler(this.Melee_player_display_Click);
            // 
            // health_player_label
            // 
            this.health_player_label.AutoSize = true;
            this.health_player_label.Location = new System.Drawing.Point(308, 121);
            this.health_player_label.Name = "health_player_label";
            this.health_player_label.Size = new System.Drawing.Size(70, 13);
            this.health_player_label.TabIndex = 26;
            this.health_player_label.Text = "Health Points";
            // 
            // health_player_display
            // 
            this.health_player_display.AutoSize = true;
            this.health_player_display.Location = new System.Drawing.Point(446, 121);
            this.health_player_display.Name = "health_player_display";
            this.health_player_display.Size = new System.Drawing.Size(13, 13);
            this.health_player_display.TabIndex = 27;
            this.health_player_display.Text = "1";
            // 
            // melee2_player_label
            // 
            this.melee2_player_label.AutoSize = true;
            this.melee2_player_label.Location = new System.Drawing.Point(512, 71);
            this.melee2_player_label.Name = "melee2_player_label";
            this.melee2_player_label.Size = new System.Drawing.Size(79, 13);
            this.melee2_player_label.TabIndex = 28;
            this.melee2_player_label.Text = "Melee 2 Hands";
            // 
            // melee2_player_display
            // 
            this.melee2_player_display.AutoSize = true;
            this.melee2_player_display.Location = new System.Drawing.Point(597, 71);
            this.melee2_player_display.Name = "melee2_player_display";
            this.melee2_player_display.Size = new System.Drawing.Size(10, 13);
            this.melee2_player_display.TabIndex = 29;
            this.melee2_player_display.Text = "-";
            // 
            // proj_player_label
            // 
            this.proj_player_label.AutoSize = true;
            this.proj_player_label.Location = new System.Drawing.Point(512, 95);
            this.proj_player_label.Name = "proj_player_label";
            this.proj_player_label.Size = new System.Drawing.Size(50, 13);
            this.proj_player_label.TabIndex = 30;
            this.proj_player_label.Text = "Projectile";
            // 
            // proj_player_display
            // 
            this.proj_player_display.AutoSize = true;
            this.proj_player_display.Location = new System.Drawing.Point(597, 95);
            this.proj_player_display.Name = "proj_player_display";
            this.proj_player_display.Size = new System.Drawing.Size(10, 13);
            this.proj_player_display.TabIndex = 31;
            this.proj_player_display.Text = "-";
            // 
            // magic_player_label
            // 
            this.magic_player_label.AutoSize = true;
            this.magic_player_label.Location = new System.Drawing.Point(512, 121);
            this.magic_player_label.Name = "magic_player_label";
            this.magic_player_label.Size = new System.Drawing.Size(36, 13);
            this.magic_player_label.TabIndex = 32;
            this.magic_player_label.Text = "Magic";
            // 
            // magic_player_display
            // 
            this.magic_player_display.AutoSize = true;
            this.magic_player_display.Location = new System.Drawing.Point(597, 121);
            this.magic_player_display.Name = "magic_player_display";
            this.magic_player_display.Size = new System.Drawing.Size(10, 13);
            this.magic_player_display.TabIndex = 33;
            this.magic_player_display.Text = "-";
            // 
            // ac_player_label
            // 
            this.ac_player_label.AutoSize = true;
            this.ac_player_label.Location = new System.Drawing.Point(633, 45);
            this.ac_player_label.Name = "ac_player_label";
            this.ac_player_label.Size = new System.Drawing.Size(62, 13);
            this.ac_player_label.TabIndex = 34;
            this.ac_player_label.Text = "Armor Class";
            // 
            // ac_player_display
            // 
            this.ac_player_display.AutoSize = true;
            this.ac_player_display.Location = new System.Drawing.Point(701, 44);
            this.ac_player_display.Name = "ac_player_display";
            this.ac_player_display.Size = new System.Drawing.Size(10, 13);
            this.ac_player_display.TabIndex = 35;
            this.ac_player_display.Text = "-";
            // 
            // separator_player
            // 
            this.separator_player.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.separator_player.Location = new System.Drawing.Point(75, 19);
            this.separator_player.Name = "separator_player";
            this.separator_player.Size = new System.Drawing.Size(688, 2);
            this.separator_player.TabIndex = 38;
            // 
            // opponent_section_title
            // 
            this.opponent_section_title.AutoSize = true;
            this.opponent_section_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.opponent_section_title.Location = new System.Drawing.Point(11, 158);
            this.opponent_section_title.Name = "opponent_section_title";
            this.opponent_section_title.Size = new System.Drawing.Size(88, 20);
            this.opponent_section_title.TabIndex = 39;
            this.opponent_section_title.Text = "Opponent";
            // 
            // separator_opponent
            // 
            this.separator_opponent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.separator_opponent.Location = new System.Drawing.Point(106, 168);
            this.separator_opponent.Name = "separator_opponent";
            this.separator_opponent.Size = new System.Drawing.Size(657, 2);
            this.separator_opponent.TabIndex = 40;
            // 
            // nbrDmgDice_oppo_label
            // 
            this.nbrDmgDice_oppo_label.AutoSize = true;
            this.nbrDmgDice_oppo_label.Location = new System.Drawing.Point(308, 194);
            this.nbrDmgDice_oppo_label.Name = "nbrDmgDice_oppo_label";
            this.nbrDmgDice_oppo_label.Size = new System.Drawing.Size(108, 13);
            this.nbrDmgDice_oppo_label.TabIndex = 59;
            this.nbrDmgDice_oppo_label.Text = "Number damage dice";
            // 
            // dmgDice_oppo_combo
            // 
            this.dmgDice_oppo_combo.FormattingEnabled = true;
            this.dmgDice_oppo_combo.Items.AddRange(new object[] {
            "4",
            "6",
            "8",
            "10",
            "12",
            "20",
            "100"});
            this.dmgDice_oppo_combo.Location = new System.Drawing.Point(204, 267);
            this.dmgDice_oppo_combo.Name = "dmgDice_oppo_combo";
            this.dmgDice_oppo_combo.Size = new System.Drawing.Size(41, 21);
            this.dmgDice_oppo_combo.TabIndex = 58;
            this.dmgDice_oppo_combo.Text = "4";
            // 
            // dmgDice_oppo_label
            // 
            this.dmgDice_oppo_label.AutoSize = true;
            this.dmgDice_oppo_label.Location = new System.Drawing.Point(118, 270);
            this.dmgDice_oppo_label.Name = "dmgDice_oppo_label";
            this.dmgDice_oppo_label.Size = new System.Drawing.Size(72, 13);
            this.dmgDice_oppo_label.TabIndex = 57;
            this.dmgDice_oppo_label.Text = "Damage Dice";
            // 
            // atckType_oppo_combo
            // 
            this.atckType_oppo_combo.FormattingEnabled = true;
            this.atckType_oppo_combo.Items.AddRange(new object[] {
            "MELEE",
            "MELEE2",
            "PROJECTILE",
            "MAGIC"});
            this.atckType_oppo_combo.Location = new System.Drawing.Point(204, 241);
            this.atckType_oppo_combo.Name = "atckType_oppo_combo";
            this.atckType_oppo_combo.Size = new System.Drawing.Size(91, 21);
            this.atckType_oppo_combo.TabIndex = 56;
            this.atckType_oppo_combo.Text = "MELEE";
            // 
            // atckType_oppo_label
            // 
            this.atckType_oppo_label.AutoSize = true;
            this.atckType_oppo_label.Location = new System.Drawing.Point(118, 244);
            this.atckType_oppo_label.Name = "atckType_oppo_label";
            this.atckType_oppo_label.Size = new System.Drawing.Size(65, 13);
            this.atckType_oppo_label.TabIndex = 55;
            this.atckType_oppo_label.Text = "Attack Type";
            // 
            // atckBonus_oppo_label
            // 
            this.atckBonus_oppo_label.AutoSize = true;
            this.atckBonus_oppo_label.Location = new System.Drawing.Point(118, 220);
            this.atckBonus_oppo_label.Name = "atckBonus_oppo_label";
            this.atckBonus_oppo_label.Size = new System.Drawing.Size(71, 13);
            this.atckBonus_oppo_label.TabIndex = 51;
            this.atckBonus_oppo_label.Text = "Attack Bonus";
            // 
            // dmgBonus_oppo_label
            // 
            this.dmgBonus_oppo_label.AutoSize = true;
            this.dmgBonus_oppo_label.Location = new System.Drawing.Point(118, 194);
            this.dmgBonus_oppo_label.Name = "dmgBonus_oppo_label";
            this.dmgBonus_oppo_label.Size = new System.Drawing.Size(80, 13);
            this.dmgBonus_oppo_label.TabIndex = 49;
            this.dmgBonus_oppo_label.Text = "Damage Bonus";
            // 
            // level_oppo_label
            // 
            this.level_oppo_label.AutoSize = true;
            this.level_oppo_label.Location = new System.Drawing.Point(12, 270);
            this.level_oppo_label.Name = "level_oppo_label";
            this.level_oppo_label.Size = new System.Drawing.Size(33, 13);
            this.level_oppo_label.TabIndex = 47;
            this.level_oppo_label.Text = "Level";
            // 
            // spirit_oppo_label
            // 
            this.spirit_oppo_label.AutoSize = true;
            this.spirit_oppo_label.Location = new System.Drawing.Point(12, 244);
            this.spirit_oppo_label.Name = "spirit_oppo_label";
            this.spirit_oppo_label.Size = new System.Drawing.Size(30, 13);
            this.spirit_oppo_label.TabIndex = 45;
            this.spirit_oppo_label.Text = "Spirit";
            // 
            // dext_oppo_label
            // 
            this.dext_oppo_label.AutoSize = true;
            this.dext_oppo_label.Location = new System.Drawing.Point(12, 220);
            this.dext_oppo_label.Name = "dext_oppo_label";
            this.dext_oppo_label.Size = new System.Drawing.Size(48, 13);
            this.dext_oppo_label.TabIndex = 43;
            this.dext_oppo_label.Text = "Dexterity";
            // 
            // strengh_oppo_label
            // 
            this.strengh_oppo_label.AutoSize = true;
            this.strengh_oppo_label.Location = new System.Drawing.Point(12, 194);
            this.strengh_oppo_label.Name = "strengh_oppo_label";
            this.strengh_oppo_label.Size = new System.Drawing.Size(44, 13);
            this.strengh_oppo_label.TabIndex = 42;
            this.strengh_oppo_label.Text = "Strengh";
            // 
            // health_oppo_label
            // 
            this.health_oppo_label.AutoSize = true;
            this.health_oppo_label.Location = new System.Drawing.Point(308, 218);
            this.health_oppo_label.Name = "health_oppo_label";
            this.health_oppo_label.Size = new System.Drawing.Size(70, 13);
            this.health_oppo_label.TabIndex = 63;
            this.health_oppo_label.Text = "Health Points";
            // 
            // melee_oppo_label
            // 
            this.melee_oppo_label.AutoSize = true;
            this.melee_oppo_label.Location = new System.Drawing.Point(308, 244);
            this.melee_oppo_label.Name = "melee_oppo_label";
            this.melee_oppo_label.Size = new System.Drawing.Size(36, 13);
            this.melee_oppo_label.TabIndex = 65;
            this.melee_oppo_label.Text = "Melee";
            // 
            // proj_oppo_label
            // 
            this.proj_oppo_label.AutoSize = true;
            this.proj_oppo_label.Location = new System.Drawing.Point(308, 270);
            this.proj_oppo_label.Name = "proj_oppo_label";
            this.proj_oppo_label.Size = new System.Drawing.Size(50, 13);
            this.proj_oppo_label.TabIndex = 67;
            this.proj_oppo_label.Text = "Projectile";
            // 
            // magic_oppo_label
            // 
            this.magic_oppo_label.AutoSize = true;
            this.magic_oppo_label.Location = new System.Drawing.Point(512, 194);
            this.magic_oppo_label.Name = "magic_oppo_label";
            this.magic_oppo_label.Size = new System.Drawing.Size(36, 13);
            this.magic_oppo_label.TabIndex = 69;
            this.magic_oppo_label.Text = "Magic";
            // 
            // AC_oppo_label
            // 
            this.AC_oppo_label.AutoSize = true;
            this.AC_oppo_label.Location = new System.Drawing.Point(512, 220);
            this.AC_oppo_label.Name = "AC_oppo_label";
            this.AC_oppo_label.Size = new System.Drawing.Size(62, 13);
            this.AC_oppo_label.TabIndex = 71;
            this.AC_oppo_label.Text = "Armor Class";
            // 
            // nbr_test_label
            // 
            this.nbr_test_label.AutoSize = true;
            this.nbr_test_label.Location = new System.Drawing.Point(19, 316);
            this.nbr_test_label.Name = "nbr_test_label";
            this.nbr_test_label.Size = new System.Drawing.Size(81, 13);
            this.nbr_test_label.TabIndex = 76;
            this.nbr_test_label.Text = "Number of tests";
            // 
            // separator_test
            // 
            this.separator_test.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.separator_test.Location = new System.Drawing.Point(178, 315);
            this.separator_test.Name = "separator_test";
            this.separator_test.Size = new System.Drawing.Size(585, 2);
            this.separator_test.TabIndex = 78;
            // 
            // output_simulation
            // 
            this.output_simulation.Location = new System.Drawing.Point(12, 374);
            this.output_simulation.Name = "output_simulation";
            this.output_simulation.ReadOnly = true;
            this.output_simulation.Size = new System.Drawing.Size(748, 124);
            this.output_simulation.TabIndex = 79;
            this.output_simulation.Text = "";
            this.output_simulation.TextChanged += new System.EventHandler(this.Output_simulation_TextChanged);
            // 
            // simulation_label
            // 
            this.simulation_label.AutoSize = true;
            this.simulation_label.Location = new System.Drawing.Point(9, 358);
            this.simulation_label.Name = "simulation_label";
            this.simulation_label.Size = new System.Drawing.Size(55, 13);
            this.simulation_label.TabIndex = 80;
            this.simulation_label.Text = "Simulation";
            // 
            // empty_button
            // 
            this.empty_button.Location = new System.Drawing.Point(494, 329);
            this.empty_button.Name = "empty_button";
            this.empty_button.Size = new System.Drawing.Size(124, 30);
            this.empty_button.TabIndex = 81;
            this.empty_button.Text = "Empty";
            this.empty_button.UseVisualStyleBackColor = true;
            this.empty_button.Click += new System.EventHandler(this.Empty_button_Click);
            // 
            // simulation_button
            // 
            this.simulation_button.Location = new System.Drawing.Point(351, 329);
            this.simulation_button.Name = "simulation_button";
            this.simulation_button.Size = new System.Drawing.Size(124, 30);
            this.simulation_button.TabIndex = 82;
            this.simulation_button.Text = "Start";
            this.simulation_button.UseVisualStyleBackColor = true;
            this.simulation_button.Click += new System.EventHandler(this.Simulation_button_Click);
            // 
            // save_button
            // 
            this.save_button.Location = new System.Drawing.Point(636, 329);
            this.save_button.Name = "save_button";
            this.save_button.Size = new System.Drawing.Size(124, 30);
            this.save_button.TabIndex = 83;
            this.save_button.Text = "Save";
            this.save_button.UseVisualStyleBackColor = true;
            this.save_button.Click += new System.EventHandler(this.Save_button_Click);
            // 
            // strengh_player_input
            // 
            this.strengh_player_input.Location = new System.Drawing.Point(62, 42);
            this.strengh_player_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.strengh_player_input.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.strengh_player_input.Name = "strengh_player_input";
            this.strengh_player_input.Size = new System.Drawing.Size(52, 20);
            this.strengh_player_input.TabIndex = 84;
            this.strengh_player_input.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.strengh_player_input.ValueChanged += new System.EventHandler(this.Strengh_player_input_ValueChanged);
            // 
            // dext_player_input
            // 
            this.dext_player_input.Location = new System.Drawing.Point(62, 67);
            this.dext_player_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.dext_player_input.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.dext_player_input.Name = "dext_player_input";
            this.dext_player_input.Size = new System.Drawing.Size(52, 20);
            this.dext_player_input.TabIndex = 85;
            this.dext_player_input.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.dext_player_input.ValueChanged += new System.EventHandler(this.Dext_player_input_ValueChanged);
            // 
            // spirit_player_input
            // 
            this.spirit_player_input.Location = new System.Drawing.Point(62, 93);
            this.spirit_player_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spirit_player_input.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spirit_player_input.Name = "spirit_player_input";
            this.spirit_player_input.Size = new System.Drawing.Size(52, 20);
            this.spirit_player_input.TabIndex = 86;
            this.spirit_player_input.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spirit_player_input.ValueChanged += new System.EventHandler(this.Spirit_player_input_ValueChanged);
            // 
            // lvl_player_input
            // 
            this.lvl_player_input.Location = new System.Drawing.Point(62, 119);
            this.lvl_player_input.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.lvl_player_input.Name = "lvl_player_input";
            this.lvl_player_input.Size = new System.Drawing.Size(52, 20);
            this.lvl_player_input.TabIndex = 87;
            this.lvl_player_input.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.lvl_player_input.ValueChanged += new System.EventHandler(this.Lvl_player_input_ValueChanged);
            // 
            // dmgbonus_player_input
            // 
            this.dmgbonus_player_input.Location = new System.Drawing.Point(204, 42);
            this.dmgbonus_player_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.dmgbonus_player_input.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.dmgbonus_player_input.Name = "dmgbonus_player_input";
            this.dmgbonus_player_input.Size = new System.Drawing.Size(52, 20);
            this.dmgbonus_player_input.TabIndex = 88;
            // 
            // bonusatck_player_input
            // 
            this.bonusatck_player_input.Location = new System.Drawing.Point(204, 67);
            this.bonusatck_player_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.bonusatck_player_input.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.bonusatck_player_input.Name = "bonusatck_player_input";
            this.bonusatck_player_input.Size = new System.Drawing.Size(52, 20);
            this.bonusatck_player_input.TabIndex = 89;
            this.bonusatck_player_input.ValueChanged += new System.EventHandler(this.Bonusatck_player_input_ValueChanged);
            // 
            // bonusCls_player_input
            // 
            this.bonusCls_player_input.Location = new System.Drawing.Point(204, 92);
            this.bonusCls_player_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.bonusCls_player_input.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.bonusCls_player_input.Name = "bonusCls_player_input";
            this.bonusCls_player_input.Size = new System.Drawing.Size(52, 20);
            this.bonusCls_player_input.TabIndex = 90;
            this.bonusCls_player_input.ValueChanged += new System.EventHandler(this.BonusCls_player_input_ValueChanged);
            // 
            // diceDmg_player_input
            // 
            this.diceDmg_player_input.Location = new System.Drawing.Point(449, 67);
            this.diceDmg_player_input.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.diceDmg_player_input.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.diceDmg_player_input.Name = "diceDmg_player_input";
            this.diceDmg_player_input.Size = new System.Drawing.Size(52, 20);
            this.diceDmg_player_input.TabIndex = 91;
            this.diceDmg_player_input.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // strengh_oppo_input
            // 
            this.strengh_oppo_input.Location = new System.Drawing.Point(62, 191);
            this.strengh_oppo_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.strengh_oppo_input.Name = "strengh_oppo_input";
            this.strengh_oppo_input.Size = new System.Drawing.Size(52, 20);
            this.strengh_oppo_input.TabIndex = 92;
            // 
            // dext_oppo_input
            // 
            this.dext_oppo_input.Location = new System.Drawing.Point(62, 217);
            this.dext_oppo_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.dext_oppo_input.Name = "dext_oppo_input";
            this.dext_oppo_input.Size = new System.Drawing.Size(52, 20);
            this.dext_oppo_input.TabIndex = 93;
            // 
            // spirit_oppo_input
            // 
            this.spirit_oppo_input.Location = new System.Drawing.Point(62, 241);
            this.spirit_oppo_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spirit_oppo_input.Name = "spirit_oppo_input";
            this.spirit_oppo_input.Size = new System.Drawing.Size(52, 20);
            this.spirit_oppo_input.TabIndex = 94;
            // 
            // lvl_oppo_input
            // 
            this.lvl_oppo_input.Location = new System.Drawing.Point(62, 267);
            this.lvl_oppo_input.Name = "lvl_oppo_input";
            this.lvl_oppo_input.Size = new System.Drawing.Size(52, 20);
            this.lvl_oppo_input.TabIndex = 95;
            // 
            // bonusDmg_oppo_input
            // 
            this.bonusDmg_oppo_input.Location = new System.Drawing.Point(204, 191);
            this.bonusDmg_oppo_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.bonusDmg_oppo_input.Name = "bonusDmg_oppo_input";
            this.bonusDmg_oppo_input.Size = new System.Drawing.Size(52, 20);
            this.bonusDmg_oppo_input.TabIndex = 96;
            // 
            // bonusAtck_oppo_input
            // 
            this.bonusAtck_oppo_input.Location = new System.Drawing.Point(204, 216);
            this.bonusAtck_oppo_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.bonusAtck_oppo_input.Name = "bonusAtck_oppo_input";
            this.bonusAtck_oppo_input.Size = new System.Drawing.Size(52, 20);
            this.bonusAtck_oppo_input.TabIndex = 97;
            // 
            // nbrDmgDice_oppo_input
            // 
            this.nbrDmgDice_oppo_input.Location = new System.Drawing.Point(422, 191);
            this.nbrDmgDice_oppo_input.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nbrDmgDice_oppo_input.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nbrDmgDice_oppo_input.Name = "nbrDmgDice_oppo_input";
            this.nbrDmgDice_oppo_input.Size = new System.Drawing.Size(52, 20);
            this.nbrDmgDice_oppo_input.TabIndex = 98;
            this.nbrDmgDice_oppo_input.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // health_oppo_input
            // 
            this.health_oppo_input.Location = new System.Drawing.Point(422, 217);
            this.health_oppo_input.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.health_oppo_input.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.health_oppo_input.Name = "health_oppo_input";
            this.health_oppo_input.Size = new System.Drawing.Size(52, 20);
            this.health_oppo_input.TabIndex = 99;
            this.health_oppo_input.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // melee_oppo_input
            // 
            this.melee_oppo_input.Location = new System.Drawing.Point(422, 241);
            this.melee_oppo_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.melee_oppo_input.Name = "melee_oppo_input";
            this.melee_oppo_input.Size = new System.Drawing.Size(52, 20);
            this.melee_oppo_input.TabIndex = 100;
            // 
            // proj_oppo_input
            // 
            this.proj_oppo_input.Location = new System.Drawing.Point(422, 267);
            this.proj_oppo_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.proj_oppo_input.Name = "proj_oppo_input";
            this.proj_oppo_input.Size = new System.Drawing.Size(52, 20);
            this.proj_oppo_input.TabIndex = 101;
            // 
            // magic_oppo_input
            // 
            this.magic_oppo_input.Location = new System.Drawing.Point(580, 191);
            this.magic_oppo_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.magic_oppo_input.Name = "magic_oppo_input";
            this.magic_oppo_input.Size = new System.Drawing.Size(52, 20);
            this.magic_oppo_input.TabIndex = 102;
            // 
            // AC_oppo_input
            // 
            this.AC_oppo_input.Location = new System.Drawing.Point(580, 216);
            this.AC_oppo_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.AC_oppo_input.Name = "AC_oppo_input";
            this.AC_oppo_input.Size = new System.Drawing.Size(52, 20);
            this.AC_oppo_input.TabIndex = 103;
            // 
            // numberTest_input
            // 
            this.numberTest_input.Location = new System.Drawing.Point(106, 314);
            this.numberTest_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numberTest_input.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numberTest_input.Name = "numberTest_input";
            this.numberTest_input.Size = new System.Drawing.Size(59, 20);
            this.numberTest_input.TabIndex = 105;
            this.numberTest_input.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // HealthInflation_player_label
            // 
            this.HealthInflation_player_label.AutoSize = true;
            this.HealthInflation_player_label.Location = new System.Drawing.Point(308, 95);
            this.HealthInflation_player_label.Name = "HealthInflation_player_label";
            this.HealthInflation_player_label.Size = new System.Drawing.Size(138, 13);
            this.HealthInflation_player_label.TabIndex = 106;
            this.HealthInflation_player_label.Text = "Health pts level up increase";
            // 
            // HealthInflation_player_input
            // 
            this.HealthInflation_player_input.Location = new System.Drawing.Point(449, 92);
            this.HealthInflation_player_input.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.HealthInflation_player_input.Name = "HealthInflation_player_input";
            this.HealthInflation_player_input.Size = new System.Drawing.Size(52, 20);
            this.HealthInflation_player_input.TabIndex = 107;
            this.HealthInflation_player_input.ValueChanged += new System.EventHandler(this.HealthInflation_player_input_ValueChanged);
            // 
            // grade_player_label
            // 
            this.grade_player_label.AutoSize = true;
            this.grade_player_label.Location = new System.Drawing.Point(633, 95);
            this.grade_player_label.Name = "grade_player_label";
            this.grade_player_label.Size = new System.Drawing.Size(62, 13);
            this.grade_player_label.TabIndex = 108;
            this.grade_player_label.Text = "Spell Grade";
            // 
            // grade_oppo_label
            // 
            this.grade_oppo_label.AutoSize = true;
            this.grade_oppo_label.Location = new System.Drawing.Point(512, 243);
            this.grade_oppo_label.Name = "grade_oppo_label";
            this.grade_oppo_label.Size = new System.Drawing.Size(62, 13);
            this.grade_oppo_label.TabIndex = 109;
            this.grade_oppo_label.Text = "Spell Grade";
            // 
            // grade_player_combo
            // 
            this.grade_player_combo.FormattingEnabled = true;
            this.grade_player_combo.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.grade_player_combo.Location = new System.Drawing.Point(704, 91);
            this.grade_player_combo.Name = "grade_player_combo";
            this.grade_player_combo.Size = new System.Drawing.Size(41, 21);
            this.grade_player_combo.TabIndex = 110;
            this.grade_player_combo.Text = "0";
            // 
            // grade_oppo_combo
            // 
            this.grade_oppo_combo.FormattingEnabled = true;
            this.grade_oppo_combo.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.grade_oppo_combo.Location = new System.Drawing.Point(580, 240);
            this.grade_oppo_combo.Name = "grade_oppo_combo";
            this.grade_oppo_combo.Size = new System.Drawing.Size(41, 21);
            this.grade_oppo_combo.TabIndex = 111;
            this.grade_oppo_combo.Text = "0";
            // 
            // ACbase_player_label
            // 
            this.ACbase_player_label.AutoSize = true;
            this.ACbase_player_label.Location = new System.Drawing.Point(633, 69);
            this.ACbase_player_label.Name = "ACbase_player_label";
            this.ACbase_player_label.Size = new System.Drawing.Size(47, 13);
            this.ACbase_player_label.TabIndex = 112;
            this.ACbase_player_label.Text = "AC base";
            // 
            // ACbase_player_combo
            // 
            this.ACbase_player_combo.FormattingEnabled = true;
            this.ACbase_player_combo.Items.AddRange(new object[] {
            "dex",
            "spi"});
            this.ACbase_player_combo.Location = new System.Drawing.Point(704, 63);
            this.ACbase_player_combo.Name = "ACbase_player_combo";
            this.ACbase_player_combo.Size = new System.Drawing.Size(41, 21);
            this.ACbase_player_combo.TabIndex = 113;
            this.ACbase_player_combo.Text = "dex";
            this.ACbase_player_combo.SelectedIndexChanged += new System.EventHandler(this.ACbase_player_combo_SelectedIndexChanged);
            // 
            // Microlite20_simulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(770, 510);
            this.Controls.Add(this.ACbase_player_combo);
            this.Controls.Add(this.ACbase_player_label);
            this.Controls.Add(this.grade_oppo_combo);
            this.Controls.Add(this.grade_player_combo);
            this.Controls.Add(this.grade_oppo_label);
            this.Controls.Add(this.grade_player_label);
            this.Controls.Add(this.HealthInflation_player_input);
            this.Controls.Add(this.HealthInflation_player_label);
            this.Controls.Add(this.numberTest_input);
            this.Controls.Add(this.AC_oppo_input);
            this.Controls.Add(this.magic_oppo_input);
            this.Controls.Add(this.proj_oppo_input);
            this.Controls.Add(this.melee_oppo_input);
            this.Controls.Add(this.health_oppo_input);
            this.Controls.Add(this.nbrDmgDice_oppo_input);
            this.Controls.Add(this.bonusAtck_oppo_input);
            this.Controls.Add(this.bonusDmg_oppo_input);
            this.Controls.Add(this.lvl_oppo_input);
            this.Controls.Add(this.spirit_oppo_input);
            this.Controls.Add(this.dext_oppo_input);
            this.Controls.Add(this.strengh_oppo_input);
            this.Controls.Add(this.diceDmg_player_input);
            this.Controls.Add(this.bonusCls_player_input);
            this.Controls.Add(this.bonusatck_player_input);
            this.Controls.Add(this.dmgbonus_player_input);
            this.Controls.Add(this.lvl_player_input);
            this.Controls.Add(this.spirit_player_input);
            this.Controls.Add(this.dext_player_input);
            this.Controls.Add(this.strengh_player_input);
            this.Controls.Add(this.save_button);
            this.Controls.Add(this.simulation_button);
            this.Controls.Add(this.empty_button);
            this.Controls.Add(this.simulation_label);
            this.Controls.Add(this.output_simulation);
            this.Controls.Add(this.separator_test);
            this.Controls.Add(this.nbr_test_label);
            this.Controls.Add(this.AC_oppo_label);
            this.Controls.Add(this.magic_oppo_label);
            this.Controls.Add(this.proj_oppo_label);
            this.Controls.Add(this.melee_oppo_label);
            this.Controls.Add(this.health_oppo_label);
            this.Controls.Add(this.nbrDmgDice_oppo_label);
            this.Controls.Add(this.dmgDice_oppo_combo);
            this.Controls.Add(this.dmgDice_oppo_label);
            this.Controls.Add(this.atckType_oppo_combo);
            this.Controls.Add(this.atckType_oppo_label);
            this.Controls.Add(this.atckBonus_oppo_label);
            this.Controls.Add(this.dmgBonus_oppo_label);
            this.Controls.Add(this.level_oppo_label);
            this.Controls.Add(this.spirit_oppo_label);
            this.Controls.Add(this.dext_oppo_label);
            this.Controls.Add(this.strengh_oppo_label);
            this.Controls.Add(this.separator_opponent);
            this.Controls.Add(this.opponent_section_title);
            this.Controls.Add(this.separator_player);
            this.Controls.Add(this.ac_player_display);
            this.Controls.Add(this.ac_player_label);
            this.Controls.Add(this.magic_player_display);
            this.Controls.Add(this.magic_player_label);
            this.Controls.Add(this.proj_player_display);
            this.Controls.Add(this.proj_player_label);
            this.Controls.Add(this.melee2_player_display);
            this.Controls.Add(this.melee2_player_label);
            this.Controls.Add(this.health_player_display);
            this.Controls.Add(this.health_player_label);
            this.Controls.Add(this.Melee_player_display);
            this.Controls.Add(this.melee_player_label);
            this.Controls.Add(this.NbrDmgDice_player_label);
            this.Controls.Add(this.dmgDice_player_combo);
            this.Controls.Add(this.dmgDice_palyer_label);
            this.Controls.Add(this.typeAt_player_combo);
            this.Controls.Add(this.typeAt_player_label);
            this.Controls.Add(this.bonusArm_player_label);
            this.Controls.Add(this.bonusAt_player_label);
            this.Controls.Add(this.bonusDam_player_label);
            this.Controls.Add(this.level_player_label);
            this.Controls.Add(this.spirit_player_label);
            this.Controls.Add(this.dexterity_player_label);
            this.Controls.Add(this.player_section_title);
            this.Controls.Add(this.strengh_player_label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Microlite20_simulator";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Microlite20_Simulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.strengh_player_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dext_player_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spirit_player_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvl_player_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dmgbonus_player_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bonusatck_player_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bonusCls_player_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceDmg_player_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strengh_oppo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dext_oppo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spirit_oppo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvl_oppo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bonusDmg_oppo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bonusAtck_oppo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbrDmgDice_oppo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.health_oppo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.melee_oppo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.proj_oppo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.magic_oppo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AC_oppo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberTest_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HealthInflation_player_input)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label strengh_player_label;
        private System.Windows.Forms.Label player_section_title;
        private System.Windows.Forms.Label dexterity_player_label;
        private System.Windows.Forms.Label spirit_player_label;
        private System.Windows.Forms.Label level_player_label;
        private System.Windows.Forms.Label bonusDam_player_label;
        private System.Windows.Forms.Label bonusAt_player_label;
        private System.Windows.Forms.Label bonusArm_player_label;
        private System.Windows.Forms.Label typeAt_player_label;
        private System.Windows.Forms.ComboBox typeAt_player_combo;
        private System.Windows.Forms.Label dmgDice_palyer_label;
        private System.Windows.Forms.ComboBox dmgDice_player_combo;
        private System.Windows.Forms.Label NbrDmgDice_player_label;
        private System.Windows.Forms.Label melee_player_label;
        private System.Windows.Forms.Label Melee_player_display;
        private System.Windows.Forms.Label health_player_label;
        private System.Windows.Forms.Label health_player_display;
        private System.Windows.Forms.Label melee2_player_label;
        private System.Windows.Forms.Label melee2_player_display;
        private System.Windows.Forms.Label proj_player_label;
        private System.Windows.Forms.Label proj_player_display;
        private System.Windows.Forms.Label magic_player_label;
        private System.Windows.Forms.Label magic_player_display;
        private System.Windows.Forms.Label ac_player_label;
        private System.Windows.Forms.Label ac_player_display;
        private System.Windows.Forms.Label separator_player;
        private System.Windows.Forms.Label opponent_section_title;
        private System.Windows.Forms.Label separator_opponent;
        private System.Windows.Forms.Label nbrDmgDice_oppo_label;
        private System.Windows.Forms.ComboBox dmgDice_oppo_combo;
        private System.Windows.Forms.Label dmgDice_oppo_label;
        private System.Windows.Forms.ComboBox atckType_oppo_combo;
        private System.Windows.Forms.Label atckType_oppo_label;
        private System.Windows.Forms.Label atckBonus_oppo_label;
        private System.Windows.Forms.Label dmgBonus_oppo_label;
        private System.Windows.Forms.Label level_oppo_label;
        private System.Windows.Forms.Label spirit_oppo_label;
        private System.Windows.Forms.Label dext_oppo_label;
        private System.Windows.Forms.Label strengh_oppo_label;
        private System.Windows.Forms.Label health_oppo_label;
        private System.Windows.Forms.Label melee_oppo_label;
        private System.Windows.Forms.Label proj_oppo_label;
        private System.Windows.Forms.Label magic_oppo_label;
        private System.Windows.Forms.Label AC_oppo_label;
        private System.Windows.Forms.Label nbr_test_label;
        private System.Windows.Forms.Label separator_test;
        private System.Windows.Forms.RichTextBox output_simulation;
        private System.Windows.Forms.Label simulation_label;
        private System.Windows.Forms.Button empty_button;
        private System.Windows.Forms.Button simulation_button;
        private System.Windows.Forms.Button save_button;
        private System.Windows.Forms.NumericUpDown strengh_player_input;
        private System.Windows.Forms.NumericUpDown dext_player_input;
        private System.Windows.Forms.NumericUpDown spirit_player_input;
        private System.Windows.Forms.NumericUpDown lvl_player_input;
        private System.Windows.Forms.NumericUpDown dmgbonus_player_input;
        private System.Windows.Forms.NumericUpDown bonusatck_player_input;
        private System.Windows.Forms.NumericUpDown bonusCls_player_input;
        private System.Windows.Forms.NumericUpDown diceDmg_player_input;
        private System.Windows.Forms.NumericUpDown strengh_oppo_input;
        private System.Windows.Forms.NumericUpDown dext_oppo_input;
        private System.Windows.Forms.NumericUpDown spirit_oppo_input;
        private System.Windows.Forms.NumericUpDown lvl_oppo_input;
        private System.Windows.Forms.NumericUpDown bonusDmg_oppo_input;
        private System.Windows.Forms.NumericUpDown bonusAtck_oppo_input;
        private System.Windows.Forms.NumericUpDown nbrDmgDice_oppo_input;
        private System.Windows.Forms.NumericUpDown health_oppo_input;
        private System.Windows.Forms.NumericUpDown melee_oppo_input;
        private System.Windows.Forms.NumericUpDown proj_oppo_input;
        private System.Windows.Forms.NumericUpDown magic_oppo_input;
        private System.Windows.Forms.NumericUpDown AC_oppo_input;
        private System.Windows.Forms.NumericUpDown numberTest_input;
        private System.Windows.Forms.Label HealthInflation_player_label;
        private System.Windows.Forms.NumericUpDown HealthInflation_player_input;
        private System.Windows.Forms.SaveFileDialog save;
        private System.Windows.Forms.Label grade_player_label;
        private System.Windows.Forms.Label grade_oppo_label;
        private System.Windows.Forms.ComboBox grade_player_combo;
        private System.Windows.Forms.ComboBox grade_oppo_combo;
        private System.Windows.Forms.Label ACbase_player_label;
        private System.Windows.Forms.ComboBox ACbase_player_combo;
    }
}

