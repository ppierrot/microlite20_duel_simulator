﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microlite20_simulator
{
    public class Opponent : Entity
    {
        public Opponent(int strengh, int dexterity, int spirit, int level, int bonus_damage, int bonus_attack, string type_attack, int damage_dice, int number_throw, int HP_max, int melee, int magic, int projectile, int ac_dext, int ac_spi) : base(strengh, dexterity, spirit, level, bonus_damage, type_attack, damage_dice, number_throw)
        {
            this.HP_Max = HP_max;
            this.Melee = melee;
            this.Melee_2hands = (int)Decimal.Floor((int)((melee + (strengh-10))));
            this.Magic = magic;
            this.Projectile = projectile;
            this.AC_dext = ac_dext;
            this.AC_spi = ac_spi;
        }
    }
}
